package com.example.android.unscramble.ui.game

import android.util.Log
import androidx.lifecycle.ViewModel

/* la variable currentScrambledWord es publica y readonly.
* puede ser accesada desde afuera de la clase ViewModel, pero no modificada.
* Solo la clase ViewModel  puede modificar el valor de _currentScrambledWord*/


class GameViewModel: ViewModel() {

    private var _score = 0
    // la variable score es publica y readonly.
    val score: Int get() = _score

    private var _currentWordCount = 0
    val currentWordCount: Int get() = _currentWordCount

    private lateinit var _currentScrambledWord: String
    val currentScrambledWord: String
        get() = _currentScrambledWord

    //words already used will be saved here
    private var wordsList: MutableList<String> = mutableListOf()
    private lateinit var currentWord: String

    //el init se lee inmediatamente luego del constructor primario de la clase y es ejecutado en el orden de lectura
    //podemos leer las variables usadas en getNextWord() porque estan declaradas antes que el init{}
    init {
        Log.d("GameViewModel", "GameViewModel created")
        getNextWord()
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("GameFragment", "GameViewModel destroyed!")
    }

    private fun getNextWord(){
        currentWord = allWordsList.random()//take a random word in the list
        //reviso si la palabra ya fue elegida, si fue asi llamo nuevamente a getNextWord()
        if(wordsList.contains(currentWord)){
            getNextWord()
        } else {
            val tempWord = currentWord.toCharArray() //transforms the word in a CharArray object
            tempWord.shuffle() //change the order of chars

            while(tempWord.toString().equals(currentWord, false)){ //compare if the two words has the same order
                tempWord.shuffle()
            }
            _currentScrambledWord = String(tempWord)
            ++_currentWordCount
            wordsList.add(currentWord)
        }
    }

    fun nextWord(): Boolean {
        return if (currentWordCount < MAX_NO_OF_WORDS){
            getNextWord()
            true
        } else false
    }

    private fun increaseScore(){
        _score += SCORE_INCREASE
    }

    fun isUserWordCorrect(playerWord: String): Boolean {
        return if (playerWord.equals(currentWord, true)){
            increaseScore()
            true
        } else false
    }

    fun reinitializeData(){
        _score = 0
        _currentWordCount = 0
        wordsList.clear()
        getNextWord()
    }

/*    private fun getNextWord() {
        currentWord = allWordsList.random()
        val tempWord = currentWord.toCharArray()
        tempWord.shuffle()

        while (tempWord.toString().equals(currentWord, false)) {
            tempWord.shuffle()
        }
        if (wordsList.contains(currentWord)) {
            getNextWord()
        } else {
            _currentScrambledWord = String(tempWord)
            ++currentWordCount
            wordsList.add(currentWord)
        }
    }*/

}